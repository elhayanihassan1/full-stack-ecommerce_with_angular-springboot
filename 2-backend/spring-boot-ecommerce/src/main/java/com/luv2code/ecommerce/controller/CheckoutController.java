package com.luv2code.ecommerce.controller;

import com.luv2code.ecommerce.dto.Purchase;
import com.luv2code.ecommerce.dto.PurchaseResponse;
import com.luv2code.ecommerce.service.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
//lorsqu'il demande une ressource provenant d'un domaine, d'un protocole ou d'un port différent de ceux utilisés pour la page courante.
@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping("/api/checkout")
public class CheckoutController {

      private CheckoutService checkoutService;

      @Autowired
      public CheckoutController(CheckoutService checkoutService){
          this.checkoutService=checkoutService;
      }

      @PostMapping("/purchase")
      public PurchaseResponse placeOrder(@RequestBody Purchase purchase){
          PurchaseResponse purchaseResponse = checkoutService.placeOrder(purchase);
          return purchaseResponse;
      }

}
