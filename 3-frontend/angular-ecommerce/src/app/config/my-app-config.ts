export default {

    oidc:{
        clientId: '0oa3yn7jeHmcRLeKL5d6',
        issuer: 'https://dev-5303645.okta.com/oauth2/default',
        redirectUri: 'http://localhost:4200/login/callback',
        scopes:   ['openid', 'profile' , 'email']
    }
}
